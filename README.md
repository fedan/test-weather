## Weather by city
### Install project
 
1. git clone git@bitbucket.org:fedan/test-weather.git
2. cd test-weather
3. composer install

### Run project
* php bin/console server:run
* go to 127.0.0.1:8000

### API:
* /api/v.1.0/city/{name}