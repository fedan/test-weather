<?php

namespace App\Service;

use App\Helper\WeatherServerInterface;

class WeatherCityService implements WeatherInterface
{
    /**
     * @var WeatherServerInterface
     */
    protected $weatherServerHelper;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * WeatherCityService constructor.
     * @param $endpoint
     * @param WeatherServerInterface $weatherServerHelper
     */
    public function __construct($endpoint, WeatherServerInterface $weatherServerHelper)
    {
        $this->endpoint = $endpoint;
        $this->weatherServerHelper = $weatherServerHelper;
    }

    /**
     * @param $city
     * @return array|mixed
     */
    public function getWeather($city)
    {
        $options = [
            'query' => [
                'q' => $city
            ]
        ];

        $data = $this->weatherServerHelper->getRemote('GET', $this->endpoint, $options);

        if ($data->getStatusCode() == '200') {
            return $this->prepareResponse($data->toArray());
        }
        return $data->getStatuscode();
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareResponse(array $data)
    {
        return [
            'city' => $data['name'],
            'country' => $data['sys']['country'],
            'wind' => $data['wind'],
            'temp' => $data['main']['temp'],
        ];
    }
}
