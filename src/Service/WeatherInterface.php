<?php


namespace App\Service;

/**
 * Interface WeatherInterface
 * @package App\Service
 */
interface WeatherInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function getWeather($data);
}
