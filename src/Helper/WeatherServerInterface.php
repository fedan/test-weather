<?php


namespace App\Helper;

/**
 * Interface WeatherServerInterface
 * @package App\Helper
 */
interface WeatherServerInterface
{
    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return mixed
     */
    public function getRemote($method, $endpoint, array $options);
}
