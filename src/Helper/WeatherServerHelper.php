<?php


namespace App\Helper;

use Symfony\Component\HttpClient\HttpClient;

/**
 * Class WeatherServerHelper
 * @package App\Helper
 */
class WeatherServerHelper implements WeatherServerInterface
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * WeatherServerHelper constructor.
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Get remote data
     * @param $method
     * @param $endpoint
     * @param array $options
     * @return \Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getRemote($method, $endpoint, array $options)
    {
        $options['query']['APPID'] = $this->apiKey;
        $options['query']['units'] = 'metric';
        $httpClient = HttpClient::create();
        return $httpClient->request($method, $endpoint, $options);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->apiKey;
    }
}
