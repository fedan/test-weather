<?php


namespace App\Controller;

use App\Service\WeatherCityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CityWeatherController
 * @package App\Controller
 */
class CityWeatherController extends AbstractController
{
    /**
     * @param WeatherCityService $cityService
     * @param $name
     * @return JsonResponse
     */
    public function index(WeatherCityService $cityService, $name)
    {
        $result = $cityService->getWeather($name);
        if (is_array($result)) {
            return new JsonResponse($result);
        }
        if ($result == JsonResponse::HTTP_NOT_FOUND) {
            return new JsonResponse('City not found', $result);
        }
        if ($result == JsonResponse::HTTP_UNAUTHORIZED) {
            return new JsonResponse('Bad token', $result);
        }
    }
}
